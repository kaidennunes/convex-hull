#!/usr/bin/python3
# why the shebang here, when it's imported?  Can't really be used stand alone, right?  And fermat.py didn't have one...
# this is 4-5 seconds slower on 1000000 points than Ryan's desktop...  Why?


from which_pyqt import PYQT_VER
if PYQT_VER == 'PYQT5':
	from PyQt5.QtCore import QLineF, QPointF, QThread, pyqtSignal
elif PYQT_VER == 'PYQT4':
	from PyQt4.QtCore import QLineF, QPointF, QThread, pyqtSignal
else:
	raise Exception('Unsupported Version of PyQt: {}'.format(PYQT_VER))

import time

class ConvexHullSolverThread(QThread):
	def __init__(self, unsorted_points,demo):
		self.points = unsorted_points					
		self.pause = demo
		QThread.__init__(self)

	def __del__(self):
		self.wait()

	show_hull = pyqtSignal(list,tuple)
	display_text = pyqtSignal(str)

# some additional thread signals you can implement and use for debugging, if you like
	show_tangent = pyqtSignal(list,tuple)
	erase_hull = pyqtSignal(list)
	erase_tangent = pyqtSignal(list)
					

	def run(self):
		assert( type(self.points) == list and type(self.points[0]) == QPointF )

		n = len(self.points)
		print( 'Computing Hull for set of {} points'.format(n) )

		t1 = time.time()

		# Sort the points by ascending x value. This method uses a Timsort strategy. The time complexity of this is O(n log n) and the space complexity is Omega(n).
		self.points.sort(key=lambda point: point.x())

		t2 = time.time()

		print('Time Elapsed (Sorting): {:3.3f} sec'.format(t2-t1))

		t3 = time.time()

		# Solve the convex hull problem
		convexHull = self.solve(self.points)

		t4 = time.time()
		
		# Pass the convex hull lines back to the GUI for display
		self.show_hull.emit(convexHull.convert_to_lines(), (255,0,0))
			
		# send a signal to the GUI thread with the time used to compute the hull
		self.display_text.emit('Time Elapsed (Convex Hull): {:3.3f} sec'.format(t4-t3))
		print('Time Elapsed (Convex Hull): {:3.3f} sec'.format(t4-t3))

	# This is a recursive algorithm whose complexity can be modeled with the master theorem: aT(ceiling(n/b)) + O(n^d). 
	# The number of subproblems is 2, each time we divide into subproblems, n is divided by 2. The time complexity of putting the parts back together is O(n). 
	# Therefore, a = 2, b = 2, and d = 1. Therefore, according to the master theorem, the time complexity of this method is O(n).
	# By the same theorem, we can calculate the space complexity, because at each part of the recursion layer, we have O(n) space complexity. By the theorem, the space complexity of this method is O(nlogn)
	def solve(self, points):
		# Find how many points there are left to be solved. This is O(1) time complexity because this property is saved in python (you don't need to iterate through the elements)
		numberOfPoints = len(points)

		# Deal with the base case. This is O(1) time complexity, since we assign just one point. Space complexity constant since we add just one point.
		if numberOfPoints == 1:
			return make_convex_hull([points[0]])

		# Divide the set of points into two subsets. This is O(n) time complexity as well as O(n) space complexity, where n is the number of points
		subsets = split_list(points)

		# Recursively solve the convex hulls for each subset
		leftSubsetConvexHull = self.solve(subsets[0])
		rightSubsetConvexHull = self.solve(subsets[1])

		# Combine and return the solved convex hulls for each subset. This, as noted in the method, has a time complexity of O(n) and a space complexity of O(n), where n is the number of points
		return self.combineConvexHulls(leftSubsetConvexHull, rightSubsetConvexHull)

	# The time complexity of this method is at O(n), since that is the most complex thing we do in it, with the while loops, as calculated below. The space complexity is O(n)
	def combineConvexHulls(self, leftSubsetConvexHull, rightSubsetConvexHull):
		# Start with the rightmost point of the left hull and the leftmost point of the right hull. These are O(n) time complexity operations
		leftSubsetConvexHull.set_current_to_rightmost_point()
		rightSubsetConvexHull.set_current_to_leftmost_point()

		# These are constant time and space complexity
		leftSidePoint = leftSubsetConvexHull.get_current_point()
		rightSidePoint = rightSubsetConvexHull.get_current_point()

		# While the calculated line isn't upper tangent to both left and right hulls, keep calculating a new tangent line. 
		# These set of while loops (this one and the two inside it) will execute at most n times, where n is the number of points in both hulls. Therefore, the time complexity of this loop is O(n). The space complexity is constant
		while True:
			# Find the original points so we can compare after calculating the new line to see if the points changed at all
			# This is constant space and time complexity
			previous_left_side_point = leftSubsetConvexHull.get_current_point()
			previous_right_side_point = rightSubsetConvexHull.get_current_point()

			# While the line isn't upper tangent to the left hull, move counter-clockwise on the left hull and calculate again
			while True:
				# Find the slope before moving to the next convex hull point. Constant time and space operation
				last_slope = (rightSidePoint.y() - leftSidePoint.y()) /  (float(rightSidePoint.x()) - leftSidePoint.x())
				
				# Move along the hull. Constant time and space operation
				leftSubsetConvexHull.move_right()
				leftSidePoint = leftSubsetConvexHull.get_current_point()

				# Find the new slope. Constant time and space operation
				current_slope = (rightSidePoint.y() - leftSidePoint.y()) /  (float(rightSidePoint.x()) - leftSidePoint.x())

				# If the slope didn't decrease, the line is upper tangent to the left hull, when using the last point. Constant time and space operation
				if last_slope <= current_slope:
					leftSubsetConvexHull.move_left()
					leftSidePoint = leftSubsetConvexHull.get_current_point()
					break
			# While the line isn't upper tangent to the right hull, move clockwise on the right hull and calculate again
			while True:
				# Find the slope before moving to the next convex hull point
				last_slope = (rightSidePoint.y() - leftSidePoint.y()) /  (float(rightSidePoint.x()) - leftSidePoint.x())

				# Move along the hull. Constant time and space operation
				rightSubsetConvexHull.move_left()
				rightSidePoint = rightSubsetConvexHull.get_current_point()
			
				# Find the new slope. Constant time and space operation
				current_slope = (rightSidePoint.y() - leftSidePoint.y()) /  (float(rightSidePoint.x()) - leftSidePoint.x())

				# If the slope didn't increase, the line is upper tangent to the right hull, when using the last point
				if last_slope >= current_slope:
					rightSubsetConvexHull.move_right()
					rightSidePoint = rightSubsetConvexHull.get_current_point()
					break
			# If the points didn't change, then we are done finding the upper tangent line
			if previous_left_side_point == leftSidePoint and previous_right_side_point == rightSidePoint:
				break
				
		# Time and space constant operation
		upper_tangent_line = QLineF(leftSidePoint, rightSidePoint)
	
		# Start with the rightmost point of the left hull and the leftmost point of the right hull. These are O(n) time complexity operations
		leftSubsetConvexHull.set_current_to_rightmost_point()
		rightSubsetConvexHull.set_current_to_leftmost_point()

		leftSidePoint = leftSubsetConvexHull.get_current_point()
		rightSidePoint = rightSubsetConvexHull.get_current_point()

		# This is the same concept as calculating the upper tangent line. While the calculated line isn't lower tangent to both left and right hulls, keep calculating a new tangent line. 
		# This loop will execute at most n times, where n is the number of points in both hulls. Therefore, the time complexity of this loop is O(n). The space complexity is constant
		while True:
			previous_left_side_point = leftSubsetConvexHull.get_current_point()
			previous_right_side_point = rightSubsetConvexHull.get_current_point()
			# While the line isn't lower tangent to the left hull, move clockwise on the right hull and calculate again
			while True:
				last_slope = (rightSidePoint.y() - leftSidePoint.y()) /  (float(rightSidePoint.x()) - leftSidePoint.x())

				leftSubsetConvexHull.move_left()
				leftSidePoint = leftSubsetConvexHull.get_current_point()
			
				current_slope = (rightSidePoint.y() - leftSidePoint.y()) /  (float(rightSidePoint.x()) - leftSidePoint.x())

				# If the slope didn't increase, the line is lower tangent to the left hull, when using the last point
				if last_slope >= current_slope:
					leftSubsetConvexHull.move_right()
					leftSidePoint = leftSubsetConvexHull.get_current_point()
					break
			# While the line isn't lower tangent to the right hull, move counter-clockwise on the right hull and calculate again
			while True:		
				last_slope = (rightSidePoint.y() - leftSidePoint.y()) /  (float(rightSidePoint.x()) - leftSidePoint.x())

				rightSubsetConvexHull.move_right()
				rightSidePoint = rightSubsetConvexHull.get_current_point()

				current_slope = (rightSidePoint.y() - leftSidePoint.y()) /  (float(rightSidePoint.x()) - leftSidePoint.x())

				# If the slope didn't decrease, the line is lower tangent to the right hull, when using the last point
				if last_slope <= current_slope:
					rightSubsetConvexHull.move_left()
					rightSidePoint = rightSubsetConvexHull.get_current_point()
					break
			# If the points didn't change, then we are done finding the upper tangent line
			if previous_left_side_point == leftSidePoint and previous_right_side_point == rightSidePoint:
				break

		# Time and space constant operation
		lower_tangent_line = QLineF(leftSidePoint, rightSidePoint)

		# Get all the points in the two convex hulls and remove all points between the tangent lines. As calculated in the method, both the space and time complexity of this call is O(n)
		combined_convex_hull_points = combine_hull_points_with_tangents(leftSubsetConvexHull, rightSubsetConvexHull, upper_tangent_line, lower_tangent_line);

		# Create and return a new convex hull with the new set of points. This is O(n) for both space and time complexity, since we have at most n elements that we need to add to the new object.
		return make_convex_hull(combined_convex_hull_points)

# Returns a tuple of first and second subsets of the given list. O(n) time complexity, because time complexity of slicing in python is equal to the slice size. O(n) space complexity, because we create two object with n elements in total
def split_list(a_list):
	middleIndex = len(a_list)//2
	return a_list[:middleIndex], a_list[middleIndex:]

# O(n) time and space complexity, where n is the number of points passed in. See ConvexHull constructor.
def make_convex_hull(new_hull_points):
	convex_hull = ConvexHull(new_hull_points)
	return convex_hull

# This method goes through all the points and makes sure that the given line is upper tangent. This means that it is O(n) time complexity, on worst case, where n is the number of points
def is_upper_tangent(line, points):
	for point in points:
		if point == line.p1() or point == line.p2():
			continue
		if (float(line.p2().x()) - line.p1().x()) * (float(point.y()) - line.p1().y()) - (float(line.p2().y()) - line.p1().y()) * (float(point.x()) - line.p1().x()) >= 0:
			return False
	return True

# This method goes through all the points and makes sure that the given line is lower tangent. This means that it is O(n) time complexity, on worst case, where n is the number of points
def is_lower_tangent(line, points):
	for point in points:
		if point == line.p1() or point == line.p2():
			continue
		if (float(line.p2().x()) - line.p1().x()) * (float(point.y()) - line.p1().y()) - (float(line.p2().y()) - line.p1().y()) * (float(point.x()) - line.p1().x()) <= 0:
			return False
	return True

# This algorithm has both a time and space complexity of O(n)
def combine_hull_points_with_tangents(first_convex_hull, second_convex_hull, upper_tangent_line, lower_tangent_line):
	# To make it easy, don't consider the individual append operations. This list can be at most n elements long, where n is the number of points in both the hulls combined. So, the space complexity for this variable will be O(n).
	points = []

	# Set the initial starting indexes for the left hull.
	first_convex_hull.set_current_to_leftmost_point()

	# Get the initial starting hull. Python doesn't create a deep copy when assigning, it only binds them. So this is a time and space constant operation
	current_convex_hull = first_convex_hull

	# Start by adding the points from the first set. We go through every point in the hulls, so this loop runs at most n times. Since all operations within the loop are time constant operations, the time complexity of the loop is O(n).
	while True:
		# Get the current point. Constant time and space complexity operation
		current_point = current_convex_hull.get_current_point()
	
		# When you reach the point you started with, you are done processing the points. Constant time complexity operation, to check a list's length and then just one point compared with another
		if len(points) > 0 and points[0] == current_point:
			break

		# Add this point to the new convex hull. See points variable for space complexity.
		points.append(current_point)

		# When you found a point that is part of a tangent line that should move to another hull, move the current point to the right, then start processing the other hull. 
		# For the assign statements, Python doesn't create a deep copy when assigning objects, it only binds them (as I do below). So these are all time and space constant operations
		if current_point == upper_tangent_line.p2():
			current_convex_hull.move_right()
			current_convex_hull = first_convex_hull		
			current_convex_hull.set_current_to_point(upper_tangent_line.p1())
		elif current_point == lower_tangent_line.p1():
			current_convex_hull.move_right()
			current_convex_hull = second_convex_hull
			current_convex_hull.set_current_to_point(lower_tangent_line.p2())
		else:
			# Move to the next point
			current_convex_hull.move_right()
	return points

class ConvexHull():
	# Space and time complexity of initializing this object is O(n), where n is the number of elements passed in.
	def __init__(self, new_hull_points):
		self.hull_points = new_hull_points
		self.current_index = 0
	
	# Time complexity is O(n), where n is the number of points in the hull
	def set_current_to_rightmost_point(self):
		for index, hull_point in enumerate(self.hull_points):
			if hull_point.x() > self.hull_points[self.current_index].x():
				self.current_index = index
	
	# Time complexity is O(n), where n is the number of points in the hull
	def set_current_to_leftmost_point(self):
		for index, hull_point in enumerate(self.hull_points):
			if hull_point.x() < self.hull_points[self.current_index].x():
				self.current_index = index
	
	# The following operations are all time and space constant operations. 
	def get_current_point(self):
		return self.hull_points[self.current_index]

	def move_left(self):
		if (self.current_index == 0):
			self.current_index = len(self.hull_points) - 1
		else:
			self.current_index -= 1

	def move_right(self):
		if (self.current_index == len(self.hull_points) - 1):
			self.current_index = 0
		else:
			self.current_index += 1

	def get_points(self):
		return self.hull_points
	
	# This is O(n) time complexity, where n is the number of points in the hull.
	def set_current_to_point(self, point):
		for index, hull_point in enumerate(self.hull_points):
			if hull_point == point:
				self.current_index = index

	# n is the number of points in the hull. There are at most n lines. So, space complexity is O(n). Since we go through every point, time complexity is O(n)
	def convert_to_lines(self):
		lines = []
		for index in range(0, len(self.hull_points) - 1):
			lines.append(QLineF(self.hull_points[index], self.hull_points[index + 1]))
		lines.append(QLineF(self.hull_points[0], self.hull_points[-1]))
		return lines